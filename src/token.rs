use core::fmt;
use std::{fmt::Debug, num::{IntErrorKind, ParseIntError}};

use logos::{Lexer, Logos};

fn bij_literal(lex: &mut Lexer<Token>) -> Result<u32, IntErrorKind> {
	let mut sum = 0;
	
	for (idx, ch) in lex.slice()[1..].chars().rev().enumerate() {
		let digit = match ch {
			'1'..='9' => ch.to_digit(10).unwrap(),
			'A' | 'a' => 10,
			_ => unreachable!(),
		};
		sum = 10_u32.checked_pow(idx as u32)
			.and_then(|index_value| index_value.checked_mul(digit))
			.and_then(|position_value| position_value.checked_add(sum))
			.ok_or(IntErrorKind::PosOverflow)?;
	}
	
	Ok(sum)
}

fn char_literal(_lex: &mut Lexer<Token>) -> Result<char, TokenError> {
	todo!()
}

pub type TokenStream<'src> = logos::Lexer<'src, Token>;

// Note: this cannot implement Copy because IntErrorKind doesn't implement it
#[derive(Debug, Clone, Default, PartialEq)]
pub enum TokenError {
	#[default]
	Unknown,
	Number(IntErrorKind),
}

impl fmt::Display for TokenError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		todo!()
	}
}

impl From<ParseIntError> for TokenError {
	fn from(value: ParseIntError) -> Self {
		Self::from(value.kind().clone())
	}
}

impl From<IntErrorKind> for TokenError {
	fn from(value: IntErrorKind) -> Self {
		Self::Number(value)
	}
}

/// The different tokens produced by the lexer.
#[derive(Logos, Debug, Clone, Copy, PartialEq, Eq)]
#[logos(error = TokenError, skip r"[ \t\r]+", skip r"#.*")]
pub enum Token {
	// keywords
	#[token("and")]    And,
	#[token("const")]  Const,
	#[token("do")]     Do,
	#[token("drop")]   Drop,
	#[token("else")]   Else,
	#[token("end")]    End,
	#[token("fi")]     Fi,
	#[token("fn")]     Fn,
	#[token("from")]   From,
	#[token("if")]     If,
	#[token("let")]    Let,
	#[token("loop")]   Loop,
	#[token("module")] Mod,
	#[token("not")]    Not,
	#[token("or")]     Or,
	#[token("proc")]   Proc,
	#[token("return")] Return,
	#[token("skip")]   Skip,
	#[token("undo")]   Undo,
	#[token("until")]  Until,
	#[token("var")]    Var,
	
	// reserved keywords
	#[token("alias")]  Alias,
	#[token("as")]     As,
	#[token("begin")]  Begin,
	#[token("done")]   Done,
	#[token("extern")] Extern,
	#[token("for")]    For,
	#[token("match")]  Match,
	#[token("struct")] Struct,
	#[token("tag")]    Tag,
	#[token("then")]   Then,
	#[token("union")]  Union,
	
	// brackets
	#[token("(")] LParen,
	#[token(")")] RParen,
	#[token("[")] LBracket,
	#[token("]")] RBracket,
	#[token("{")] LBrace,
	#[token("}")] RBrace,
	
	// relational
	#[token("=")]  Eq,
	#[token("!=")] Neq,
	#[token("<")]  Lt,
	#[token(">")]  Gt,
	#[token("<=")] Lte,
	#[token(">=")] Gte,
	
	// assignments
	#[token("<>")]
	#[token("<=>")]
	Swap,
	#[token(":=")] Assign,
	#[token("+=")] AddAssign,
	#[token("-=")] SubAssign,
	//#[token("*=")] MulAssign,
	//#[token("/=")] DivAssign,
	#[token(":<")] RolAssign,
	#[token(":>")] RorAssign,
	#[token("^=")] XorAssign,
	
	// multi-purpose
	#[token(":")] Colon,
	#[token(".")] Period,
	#[token(",")] Comma,
	#[token(";")] Semicolon,
	
	#[token("+")] Plus,
	#[token("-")] Minus,
	#[token("*")] Star,
	#[token("/")] FSlash,
	#[token("%")] Percent,
	#[token("!")] Bang,
	#[token("^")] Caret,
	#[token("~")] Tilde,
	#[token("_")] Underscore,
	
	// unused
	#[token("::")] Scope,
	#[token("->")] RightArrow,
	#[token("?")]  QMark,
	#[token(":-")] Impls,
	
	// v important
	#[token("\n")] Newline,
	
	// identifiers
	
	#[regex("[a-z_][A-Za-z0-9_]*")]
	VarIdent,
	#[regex("[A-Z][A-Za-z0-9_]*")]
	ConIdent,
	
	// literals
	
	#[regex("[1-9][0-9]*", |lex| u32::from_str_radix(lex.slice(), 10))]
	#[regex("0x[[:xdigit:]]+", |lex| u32::from_str_radix(&lex.slice()[2..], 16))]
	#[regex("0b[01]+", |lex| u32::from_str_radix(&lex.slice()[2..], 2))]
	#[regex("0[1-9aA]*", bij_literal)]
	Number(u32),
	
	#[regex(r"'(\\[ntr0'\\]|\\x[[:xdigit:]]{2}|[^'\\\n\t\r])'")]
	Char,
	
	#[regex(r#""(\\[ntr0"\\]|[^"\\])*""#)]
	#[regex(r#"“(\\[ntr0”\\]|[^”\\])*”"#)]
	#[regex(r#"«(\\[ntr0»\\]|[^»\\])*»"#)]
	String,
}
