#![allow(dead_code)]

use std::{io::prelude::*, sync::LazyLock};
use std::sync::Mutex;

use super::{EvalResult, EvalError, Value, io::{RevStdout, RevStdin}};
use crate::ast::{Param, ProcDef, Procedure, Type};

static RSTDOUT: LazyLock<Mutex<RevStdout>> = LazyLock::new(|| Mutex::new(RevStdout::new()));
//static STDIN: LazyLock<Mutex<RevStdin>> = LazyLock::new(|| Mutex::new(RevStdin::new()));

// Arguments: str:String, bytes:Uint
// Action: moves str to stdout, increments bytes by number of bytes written.
pub fn print(args: &mut [Value]) -> EvalResult<()> {
	if let [Value::String(string), Value::U32(bytes)] = args {
		let mut rstdout = RSTDOUT.lock().unwrap();
		let bytes_read = rstdout.write(string.as_bytes()).unwrap();
		*string = string.split_off(bytes_read);
		*bytes += bytes_read as u32;
		Ok(())
	} else {
		// type checking and parity handled by procedure evaluation.
		unreachable!()
	}
}

// Arguments: str:String, bytes:Uint
// Action: decrements bytes by number of bytes that will be read, and moves
//         stdout data into str.
pub fn unprint(args: &mut [Value]) -> EvalResult<()> {
	if let [Value::String(string), Value::U32(bytes)] = args {
		let mut rstdout = RSTDOUT.lock().unwrap();
		let s = String::from_utf8(rstdout.unwrite(*bytes as usize)).unwrap();
		*bytes -= s.len() as u32;
		*string = s + &string;
		Ok(())
	} else {
		// type checking and parity handled by procedure evaluation.
		unreachable!()
	}
}

pub static PROC_PRINT: LazyLock<Procedure> = LazyLock::new(|| Procedure {
	name: "print".into(),
	params: vec![
		Param {
			name: "msg".into(),
			constant: false,
			typ: Type::String,
		},
		Param {
			name: "bytes_read".into(),
			constant: false,
			typ: Type::U32,
		},
	],
	code: ProcDef::Internal {
		fore: print,
		back: unprint,
	},
});


/*
pub fn show(args: &mut [Value]) -> EvalResult<()> {
	assert!(args.len() == 1);
	
	let mut stdout = RSTDOUT.lock().unwrap();
	
	if let Value::String(string) = &args[0] {
		stdout.write(string.as_bytes()).unwrap();
		Ok(())
	} else {
		Err(EvalError::TypeMismatch {
			expected: Type::String,
			got: args[0].get_type(),
		})
	}
}

pub fn unshow(args: &mut [Value]) -> EvalResult<()> {
	assert!(args.len() == 1);
	
	let mut stdout = RSTDOUT.lock().unwrap();
	
	if let Value::String(string) = &args[0] {
		let extracted_data = stdout.unwrite(string.len());
		assert_eq!(string.as_bytes(), extracted_data.as_slice());
		Ok(())
	} else {
		Err(EvalError::TypeMismatch {
			expected: Type::String,
			got: args[0].get_type(),
		})
	}
}

pub static PROC_SHOW: LazyLock<Procedure> = LazyLock::new(|| Procedure {
	name: "show".to_string(),
	params: vec![
		Param {
			name: "string".to_string(),
			constant: false,
			typ: Type::String,
		}
	],
	code: ProcDef::Internal {
		fore: intrinsic::show,
		back: intrinsic::unshow
	},
});
*/
