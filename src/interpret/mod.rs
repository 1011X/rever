//use std::io::prelude::*;
use std::fmt;

use crate::ast::{self, Item, Module, Type, Procedure, Param, ProcDef};

pub use self::value::Value;
pub use self::stack::{Stack, StackFrame, Context};

mod io;
mod value;
pub mod intrinsic;
mod stack;

//pub type Scope = Vec<(String, Value)>;
pub type EvalResult<T> = Result<T, EvalError>;

#[derive(Debug)]
pub enum EvalError {
	TypeMismatch {
		expected: Type,
		got: Type,
	},
	UnknownIdent(String),
	IrreversibleState,
}

impl fmt::Display for EvalError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			EvalError::UnknownIdent(id) =>
				write!(f, "name {:?} is not defined", id),
			EvalError::TypeMismatch { expected, got } =>
				write!(f, "expected {:?}, got {:?}", expected, got),
			EvalError::IrreversibleState =>
				f.write_str("hit an irreversible state"),
		}
	}
}

// Creates root module, loads intrinsics, finds `main`, and executes it.
pub fn interpret_file(items: Vec<ast::Item>) {
	// create root module
	let mut root_mod = Module::new("root".into(), items);
		
	root_mod.items.push(
		Item::Proc(crate::interpret::intrinsic::PROC_PRINT.clone())
	);
	
	// find main procedure
	let main = root_mod.items.iter()
		.find(|item| matches!(item, Item::Proc(pr) if pr.name == "main"));
	
	// TODO set up stack
	let root = Context::from_module(&root_mod);
	
	// run main procedure, if any
	if let Some(Item::Proc(pr)) = main {
		pr.call(root, Vec::new()).unwrap();
	} else {
		eprintln!("No main procedure found.");
	}
}
